# paths

path_r_python = function(x, y = NULL) { 
  if (!is.null(y)) { 
    if (language == "r") { 
      path = file.path(x, y)
    }
    if (language == "python") {
      path = os.pathjoin(x, y)
    }
  return(path)
  }
  if (is.null(y)) { 
    if (language == "r") { 
      path = file.path(x)
    }
    if (language == "python") {
      path = os.pathjoin(x)
    }
    return(path)
  }
}

## config
config = path_r_python("../config")

### default

config_default = path_r_python(config, "default")

#### inform  

config_default_inform = path_r_python(config_default, "inform_default.yaml")

### params

config_params = path_r_python(config, "params")

#### inform  

config_params_inform = path_r_python(config_params, "inform_params.yaml")

## data

data = path_r_python("../data")

### source

data_source = path_r_python(data, "source")

#### dictionaries

data_source_dictionaries = path_r_python(data_source, "dictionaries")

##### ranks

data_source_dictionaries_ranks = path_r_python(data_source_dictionaries, "ranks.tsv")

### interim

data_interim = path_r_python(data, "interim")

#### features

data_interim_features = path_r_python(data_interim, "features")

#### organisms

data_interim_organisms = path_r_python(data_interim, "organisms")

##### original

data_interim_organisms_original = path_r_python(data_interim_organisms, "original.tsv")

data_interim_organisms_original_2 = path_r_python(data_interim_organisms, "original_2.tsv")

##### cleaned

data_interim_organisms_cleaned = path_r_python(data_interim_organisms, "cleaned.json")

##### verified

data_interim_organisms_verified = path_r_python(data_interim_organisms, "verified.json")

##### verified 2

data_interim_organisms_verified_2 = path_r_python(data_interim_organisms, "verified_2.json")

### processed

data_processed = path_r_python(data, "processed")

#### params 

data_processed_params = path_r_python(data_processed, "params")

##### inform 

data_processed_params_inform = path_r_python(data_processed_params, "inform_params.yaml")

## src

### gnfinder script

gnfinder_script = "shell/gnfinderScript.sh"

### gnverifier script

gnverifier_script = "shell/gnverifierScript.sh"

### gnverifier script 2

gnverifier_script_2 = "shell/gnverifierScript_2.sh"

### docopt

#### inform
docopt_2_inform_features_taxa = "docopt/inform_features_taxa.txt"
