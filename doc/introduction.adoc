== Introduction

This repository contains everything needed to inform taxonomically your features to perform taxonomically informed metabolite annotation.

It needs two tables:

* A feature table with samples as columns
* A metadata table with column name -> biological origin correspondance
** The metadata table can come from GNPS or be created manually (see config)

It then attributes the biological source to the N (see config) columns with the highest intensities.
This way, you are able to have one (or more) biological sources per feature.

We favor the Open Tree of Life (https://tree.opentreeoflife.org/) taxonomy, as it is probably the most exhaustive one and also the one included within link:LOTUS[https://lotusnprod.github.io/lotus-manuscript/], so you'll have better matches.

You can add your own, you simply need to stick to the same model we use.
