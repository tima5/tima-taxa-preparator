= Taxonomically Informed Metabolite Annotation: taxa-Preparator
:toc:

include::doc/introduction.adoc[]

include::doc/instructions.adoc[]

include::doc/docker.adoc[]

include::doc/tests.adoc[]

include::doc/acknowledgments.adoc[]

include::doc/references.adoc[]
