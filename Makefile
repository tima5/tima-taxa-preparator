include config.mk

.PHONY: get-gnfinder get-gnverifier get-bins inform-taxa
.PRECIOUS: %.tsv %.zip %.json %.gz

get-bins: get-gnfinder get-gnverifier

get-gnfinder: bin/gnfinder
get-gnverifier: bin/gnverifier

bin/gnfinder: ${BIN_PATH}/gnfinder
${BIN_PATH}/gnfinder: config.mk
	mkdir -p bin
	curl -L https://github.com/gnames/gnfinder/releases/download/${GNFINDER_VERSION}/gnfinder-${GNFINDER_VERSION}-${PLATFORM}.tar.gz | tar xOz gnfinder > bin/gnfinder
	chmod +x bin/gnfinder

bin/gnverifier: ${BIN_PATH}/gnverifier
${BIN_PATH}/gnverifier: config.mk
	mkdir -p bin
	curl -L https://github.com/gnames/gnverifier/releases/download/${GNVERIFIER_VERSION}/gnverifier-${GNVERIFIER_VERSION}-${PLATFORM}.tar.gz | tar xOz gnverifier > bin/gnverifier
	chmod +x bin/gnverifier

inform-taxa:
	cd src && Rscript R/inform_features_taxa.R