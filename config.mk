export BIN_PATH ?= ${PWD}/bin
export CONFIG_PATH ?= ${PWD}/config
export DATA_PATH ?= ${PWD}/data
export DOC_PATH ?= ${PWD}/doc
export SRC_PATH ?= ${PWD}/src
export TESTS_PATH ?= ${PWD}/tests

export GNFINDER_VERSION = v0.14.1
export GNVERIFIER_VERSION = v0.3.1

export UNAME := $(shell uname)

PLATFORM := unsupported
ifeq ($(OS),Windows_NT)
	PLATFORM := unsupported
else
	UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        PLATFORM := linux
    endif
    ifeq ($(UNAME_S),Darwin)
        PLATFORM := mac
    endif
endif
