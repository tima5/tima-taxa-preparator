#!/usr/bin/env bash

echo "Let's check if we can run the whole process"
echo " Get bins"
make get-bins
echo " Inform taxa"
make inform-taxa

echo "We need to do quality controls, please help!"
